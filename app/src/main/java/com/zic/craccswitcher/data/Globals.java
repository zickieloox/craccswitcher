package com.zic.craccswitcher.data;

import android.annotation.SuppressLint;

public class Globals {

    // Clash Royale shared_prefs directory path
    @SuppressLint("SdCardPath")
    public final static String CLASH_ROYALE_DATA_PATH = "/data/data/com.supercell.clashroyale/shared_prefs";
    public final static String CLASH_ROYALE_PACKAGE_NAME = "com.supercell.clashroyale";

    public final static String KEY_ACC_PATH = "accPath";
    public final static String KEY_ACC_NAME = "accName";
}
