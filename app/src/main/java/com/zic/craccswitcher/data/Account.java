package com.zic.craccswitcher.data;

public class Account {
    //private int id;
    private String accPath;
    private String accDataPath;
    private String accName;
    private boolean switched = false;

    public Account(String accPath, String accDataPath, String accName) {
        this.accDataPath = accDataPath;
        this.accPath = accPath;
        this.accName = accName;
    }

    public String getAccDataPath() {
        return accDataPath;
    }

    public void setAccDataPath(String accDataPath) {
        this.accDataPath = accDataPath;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getAccPath() {
        return accPath;
    }

    public boolean isSwitched() {
        return switched;
    }

    public void setSwitched(boolean switched) {
        this.switched = switched;
    }
}
