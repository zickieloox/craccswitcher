package com.zic.craccswitcher.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.zic.craccswitcher.R;
import com.zic.craccswitcher.adapter.AccountAdapter;
import com.zic.craccswitcher.data.Account;
import com.zic.craccswitcher.data.Globals;
import com.zic.craccswitcher.dialog.NewAccountDialogFragment;
import com.zic.craccswitcher.listener.OnDeleteDialogClickListener;
import com.zic.craccswitcher.listener.OnRenameDialogClickListener;
import com.zic.craccswitcher.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AccountRecyclerFragment extends Fragment implements OnRenameDialogClickListener, OnDeleteDialogClickListener {
    private static final String TAG = "AccountRecyclerFragment";
    private static final String ADD_DIALOG = "Add Dialog";
    SwipeRefreshLayout refreshLayout;
    AccountAdapter accAdapter;
    private List<Account> accounts = new ArrayList<>();
    private RecyclerView accountRecycler;
    private String accPath;

    public AccountRecyclerFragment() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_account_list, container, false);

        accPath = getActivity().getFilesDir().getAbsolutePath() + "/Accounts";
        final FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadAccounts();
            }
        });

        accountRecycler = (RecyclerView) rootView.findViewById(R.id.accountRecycler);
        loadAccounts();

        accountRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    fab.hide();
                } else {
                    fab.show();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        accountRecycler.setLayoutManager(new LinearLayoutManager(container.getContext()));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewAccountDialogFragment f = new NewAccountDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Globals.KEY_ACC_PATH, accPath);
                f.setArguments(bundle);
                f.show(getActivity().getSupportFragmentManager(), ADD_DIALOG);
            }
        });

        return rootView;
    }

    private void loadAccounts() {
        boolean empty = false;
        accounts.clear();

        File file = new File(accPath);
        if (!file.exists()) {
            empty = true;
            file.mkdir();
        } else {
            try {
                // List all directory in $accPath
                for (File sub : file.listFiles()) {
                    if (sub.isDirectory())
                        accounts.add(new Account(accPath, sub.getAbsolutePath(), sub.getName()));
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.toString());
                Toast.makeText(getActivity(), getString(R.string.toast_err_exception), Toast.LENGTH_SHORT).show();
                return;
            }
        }

        // Check $accounts is empty or not
        if (accounts.isEmpty() || empty) {
            Toast.makeText(getActivity(), getString(R.string.toast_no_acc), Toast.LENGTH_SHORT).show();
        }

        setupAdapter();
    }

    private void setupAdapter() {
        accAdapter = new AccountAdapter(getContext(), AccountRecyclerFragment.this, getActivity().getSupportFragmentManager(), accounts);
        accountRecycler.setAdapter(accAdapter);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onRenameClick(String oldAccName, String newAccName) {
        Account curAccount = accAdapter.getCurItem();
        String accPath = curAccount.getAccPath();

        String newAccDataPath = accPath + "/" + newAccName;

        // Check if the name has already been existed
        if (Utils.exists(newAccDataPath, true)) {
            Toast.makeText(getActivity(), getString(R.string.toast_existed_name), Toast.LENGTH_SHORT).show();
            return;
        }

        // Rename the old $accDataPath to $newAccName
        if (!Utils.renameAsRoot(accPath, oldAccName, newAccName)) {
            Toast.makeText(getActivity(), getString(R.string.toast_err_rename), Toast.LENGTH_SHORT).show();
            return;
        }

        // Update the list
        accAdapter.renameItem(curAccount, newAccDataPath, newAccName);
    }

    @Override
    public void onDeleteClick() {
        Account curAccount = accAdapter.getCurItem();
        String accName = curAccount.getAccName();
        String accDataPath = curAccount.getAccDataPath();

        if (Utils.deleteAsRoot(accDataPath)) {
            // Update the list
            accAdapter.removeItem(curAccount);
        } else {
            Toast.makeText(getActivity(), getString(R.string.toast_err_delete) + " \"" + accName + "\"", Toast.LENGTH_SHORT).show();
        }
    }
}
