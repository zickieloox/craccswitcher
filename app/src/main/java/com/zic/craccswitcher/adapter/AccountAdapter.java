package com.zic.craccswitcher.adapter;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.zic.craccswitcher.R;
import com.zic.craccswitcher.data.Account;
import com.zic.craccswitcher.data.Globals;
import com.zic.craccswitcher.dialog.DeleteDialogFragment;
import com.zic.craccswitcher.dialog.RenameDialogFragment;
import com.zic.craccswitcher.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.ViewHolder> implements View.OnClickListener {

    private static final String RENAME_DIALOG = "Rename Dialog";
    private static final String DELETE_DIALOG = "Delete Dialog";

    private Context context;
    private Fragment frag;
    private FragmentManager fragMan;

    private List<Account> items = new ArrayList<>();
    private Account curItem;

    public AccountAdapter(Context context, Fragment fragment, FragmentManager fragmentManager, List<Account> values) {
        this.context = context;
        this.frag = fragment;
        this.fragMan = fragmentManager;
        this.items = values;
        sortAccounts(items);
    }

    private void sortAccounts(List<Account> accounts) {
        // Sort the $accounts by name without capital
        Collections.sort(accounts, new Comparator<Account>() {
            @Override
            public int compare(Account o1, Account o2) {
                return o1.getAccName().toLowerCase().compareTo(o2.getAccName().toLowerCase());
            }
        });
    }

    public Account getCurItem() {
        return curItem;
    }

    public void removeItem(Account acc) {
        int pos = items.indexOf(acc);
        items.remove(acc);
        notifyItemRemoved(pos);
        notifyItemRangeChanged(pos, items.size());
    }

    public void renameItem(Account acc, String newAccDataPath, String newAccName) {
        int pos = items.indexOf(acc);
        items.get(pos).setAccDataPath(newAccDataPath);
        items.get(pos).setAccName(newAccName);
        sortAccounts(items);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_account, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(items.get(position));
        holder.itemView.setOnClickListener(this);
        holder.tvAccName.setText(position + 1 + ". " + items.get(position).getAccName());
        holder.imgMoreIcon.setTag(items.get(position));
        holder.imgMoreIcon.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onClick(final View v) {
        int id = v.getId();
        switch (id) {
            case R.id.moreIconImageButton:
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        showPopupMenu(v);
                    }
                });
                break;
            default:
                switchAccount(v);
        }
    }

    private void switchAccount(View v) {
        curItem = (Account) v.getTag();
        String crDataPath = Globals.CLASH_ROYALE_DATA_PATH;
        String crPackageName = Globals.CLASH_ROYALE_PACKAGE_NAME;
        String accDataPath = curItem.getAccDataPath();
        String accName = curItem.getAccName();

        // Check Clash Royale shared_prefs directory exists or not
        if (!Utils.existsWithRoot(crDataPath, true)) {
            Toast.makeText(context, context.getString(R.string.toast_install_launch), Toast.LENGTH_SHORT).show();
            return;
        }

        // Kill Clash Royale before copy account's data
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        activityManager.killBackgroundProcesses(crPackageName);

        // Copy shared_prefs files: storage_new.xml & storage.xml to $crDataPath
        if (!Utils.copyFileAsRoot(accDataPath, crDataPath, "storage.xml") || !Utils.copyFileAsRoot(accDataPath, crDataPath, "storage_new.xml")) {
            Toast.makeText(context, context.getString(R.string.toast_err_file), Toast.LENGTH_SHORT).show();
            return;
        }

        Toast.makeText(context, "\"" + accName + "\" " + context.getString(R.string.toast_switched), Toast.LENGTH_SHORT).show();

        // Launch Clash Royale
        PackageManager pm = context.getPackageManager();
        Intent intentCr = pm.getLaunchIntentForPackage(crPackageName);
        if (intentCr != null) {
            context.startActivity(intentCr);
        } else {
            Toast.makeText(context, context.getString(R.string.toast_err_launch), Toast.LENGTH_SHORT).show();
        }
    }

    private void showPopupMenu(final View v) {

        curItem = (Account) v.getTag();

        PopupMenu popup = new PopupMenu(context, v);
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_rename:
                        Bundle bundle = new Bundle();
                        bundle.putString(Globals.KEY_ACC_NAME, curItem.getAccName());

                        RenameDialogFragment frag1 = new RenameDialogFragment();
                        frag1.setTargetFragment(frag, 0);
                        frag1.setArguments(bundle);
                        frag1.show(fragMan, RENAME_DIALOG);
                        return true;

                    case R.id.menu_delete:
                        DeleteDialogFragment frag2 = new DeleteDialogFragment();
                        frag2.setTargetFragment(frag, 0);
                        frag2.show(fragMan, DELETE_DIALOG);
                        return true;
                }
                return false;
            }
        });

        popup.show();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private View itemView;
        //private ImageView imgAccIcon;
        private TextView tvAccName;
        private ImageButton imgMoreIcon;

        ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            //imgAccIcon = (ImageView) itemView.findViewById(R.id.accIconImageView);
            tvAccName = (TextView) itemView.findViewById(R.id.accNameTextView);
            imgMoreIcon = (ImageButton) itemView.findViewById(R.id.moreIconImageButton);
        }
    }

}
