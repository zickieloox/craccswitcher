package com.zic.craccswitcher.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.zic.craccswitcher.R;
import com.zic.craccswitcher.data.Globals;
import com.zic.craccswitcher.listener.OnRenameDialogClickListener;

public class RenameDialogFragment extends android.support.v4.app.DialogFragment {
    private OnRenameDialogClickListener callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            callback = (OnRenameDialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("The calling fragment must implement OnRenameDialogClickListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_rename, null);
        final TextInputLayout tilNewAccName = (TextInputLayout) view.findViewById(R.id.newAccNameTIL);

        Bundle bundle = getArguments();
        final String oldAccName = bundle.getString(Globals.KEY_ACC_NAME);

        builder.setTitle(getString(R.string.dialog_rename_title));
        builder.setView(view);

        assert tilNewAccName.getEditText() != null;
        tilNewAccName.getEditText().setText(oldAccName);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newAccName = tilNewAccName.getEditText().getText().toString().trim();
                if (newAccName.length() == 0) {
                    Toast.makeText(getActivity(), "New account name cannot be empty!", Toast.LENGTH_SHORT).show();
                    return;
                }

                callback.onRenameClick(oldAccName, newAccName);

            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        return builder.create();
    }

}
