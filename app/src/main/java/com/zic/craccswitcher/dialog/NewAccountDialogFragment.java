package com.zic.craccswitcher.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.zic.craccswitcher.R;
import com.zic.craccswitcher.activity.MainActivity;
import com.zic.craccswitcher.data.Globals;
import com.zic.craccswitcher.utils.Utils;

import java.io.File;

public class NewAccountDialogFragment extends DialogFragment {
    private String accPath;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_new_account, null);

        final TextInputLayout tilAccName = (TextInputLayout) view.findViewById(R.id.accNameTIL);

        builder.setTitle("Add current account");
        builder.setView(view);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                assert tilAccName.getEditText() != null;
                String accNameValue = tilAccName.getEditText().getText().toString().trim();
                if (accNameValue.length() == 0) {
                    Toast.makeText(getActivity(), "Account name cannot be empty!", Toast.LENGTH_SHORT).show();
                    return;
                }

                Bundle bundle = getArguments();
                accPath = bundle.getString(Globals.KEY_ACC_PATH);

                if (!addNewAccount(accNameValue)) {
                    return;
                }

                // Refresh MainActivity
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();

            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        return builder.create();
    }

    public boolean addNewAccount(String accName) {
        String newAccDataPath = accPath + "/" + accName;
        File file = new File(newAccDataPath);

        if (Utils.exists(newAccDataPath, true)) {
            Toast.makeText(getActivity(), getString(R.string.toast_existed_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!file.mkdir()) {
            return false;
        }

        String crDataPath = Globals.CLASH_ROYALE_DATA_PATH;
        if (!Utils.copyFileAsRoot(crDataPath, newAccDataPath, "storage.xml") || !Utils.copyFileAsRoot(crDataPath, newAccDataPath, "storage_new.xml")) {
            Toast.makeText(getActivity(), getString(R.string.toast_err_file), Toast.LENGTH_SHORT).show();
            // Delete unused $newAccDataPath directory
            Utils.deleteDir(newAccDataPath);
            return false;
        }

        return true;
    }
}
