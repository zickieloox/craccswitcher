package com.zic.craccswitcher.utils;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class Utils {

    private static final String TAG = "Utils";
    private static String exeOutput, exeError;

    public static Boolean exe(String cmd, boolean asRoot) {
        StringBuilder out = new StringBuilder();
        StringBuilder err = new StringBuilder();
        try {
            String line;

            Process process;

            if (asRoot) {
                process = Runtime.getRuntime().exec("su");
            } else {
                process = Runtime.getRuntime().exec(cmd);
            }

            OutputStream stdin = process.getOutputStream();
            InputStream stderr = process.getErrorStream();
            InputStream stdout = process.getInputStream();

            if (asRoot) {
                stdin.write((cmd + "\n").getBytes());

            }
            stdin.write("exit\n".getBytes());
            stdin.flush();
            stdin.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
            while ((line = br.readLine()) != null) {
                out.append(line).append("\n");
            }
            br.close();

            br = new BufferedReader(new InputStreamReader(stderr));
            while ((line = br.readLine()) != null) {
                err.append(line).append("\n");
            }
            br.close();

            process.waitFor();
            process.destroy();
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.toString());
            exeError = "Exception";
            return false;
        }
        exeOutput = out.toString().trim(); //Trim to remove \n
        exeError = err.toString().trim();

        // Commands is executable ot not
        return exeError.length() == 0;
    }

    static String getExeOutput() {
        return exeOutput;
    }

    static String getExeError() {
        return exeError;
    }

    public static boolean copyFileAsRoot(String srcDir, String desDir, String fileName) {

        String srcFile = (srcDir + "/" + fileName);
        String desFile = (desDir + "/" + fileName);
        srcFile = getValid(srcFile);
        desFile = getValid(desFile);

        if (exe("cat " + srcFile + " > " + desFile, true)) {
            return true;
        } else
            Log.e(TAG, "copyFileAsRoot: " + getExeError());
        return false;
    }

    public static boolean exists(String filePath, boolean isDir) {
        File file = new File(filePath);
        if (isDir) {
            return file.exists() && file.isDirectory();
        } else {
            return file.exists();
        }
    }

    public static boolean existsWithRoot(String filePath, boolean isDir) {
        String cmdToExecute = "ls" + (isDir ? " -d " : " ") + filePath;
        if (exe(cmdToExecute, true)) {
            if (getExeOutput().equals(filePath)) {
                return true;
            }
        }

        return false;
    }

    // Rename a file or dir as root (no read/write permission error)
    public static boolean renameAsRoot(String dirPath, String oldName, String newName) {
        String oldFilePath = getValid(dirPath + "/" + oldName);
        String newFilePath = getValid(dirPath + "/" + newName);
        if (exe("mv " + oldFilePath + " " + newFilePath, true)) {
            return true;
        } else {
            Log.e(TAG, "renameAsRoot: " + getExeError());
            return false;
        }
    }

    // Delete a file or dir as root (no read/write permission error)
    public static boolean deleteAsRoot(String filePath) {
        filePath = getValid(filePath);
        return exe("rm -rf " + filePath, true);
    }

    // Checks if external storage is available for read and write
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    // The read/write permission error maybe occurred
    public static boolean deleteDir(String dirPath) {
        File file = new File(dirPath);
        if (!file.isDirectory()) {
            Log.e(TAG, "deleteDir: " + dirPath + " is not a directory.");
        }
        for (File sub : file.listFiles()) {
            sub.delete();
        }
        return file.delete();
    }

    // A function make the target String to a valid String works with Shell Commands - replace all spaces with " "
    private static String getValid(String targetString) {
        return targetString.replaceAll("\\s+", "\" \"");
    }
}
