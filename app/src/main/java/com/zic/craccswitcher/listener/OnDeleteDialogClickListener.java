package com.zic.craccswitcher.listener;


public interface OnDeleteDialogClickListener {
    void onDeleteClick();
}
