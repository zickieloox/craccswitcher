package com.zic.craccswitcher.listener;

public interface OnRenameDialogClickListener {
    void onRenameClick(String oldAccName, String newAccName);
}
