package com.zic.craccswitcher.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.zic.craccswitcher.R;
import com.zic.craccswitcher.fragment.AccountRecyclerFragment;
import com.zic.craccswitcher.utils.Utils;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Check read & write
        if (!Utils.isExternalStorageWritable()) {
            Toast.makeText(this, getString(R.string.toast_err_write), Toast.LENGTH_SHORT).show();
            return;
        }

        // Get root access first
        if (!Utils.exe((""), true)) {
            Toast.makeText(this, getString(R.string.toast_err_root), Toast.LENGTH_SHORT).show();
            return;
        }

        AccountRecyclerFragment accountRecyclerFragment = new AccountRecyclerFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, accountRecyclerFragment).commit();

        //final String accBackupPath = Environment.getExternalStorageDirectory().getPath() + "/Zickie/Clash Royale";

    }
}
